all: issue.vvp issue.vpi
	vvp -m issue -M . issue.vvp

issue.vvp: tb_top.v
	iverilog -o issue.vvp tb_top.v

issue.vpi: issue_vpi.c
	gcc -shared -fPIC -o issue.vpi -I/usr/include/iverilog/ issue_vpi.c

clean:
	rm -f *.vvp *.vpi
