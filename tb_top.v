module tb_top;
reg clk = 0;
reg a = 0, b = 0;
wire c;
assign c = a & b;

initial #1 clk = 1;

always @ (posedge clk)
  $display("a = ", a, ", ",
           "b = ", b, ", ",
           "c = ", c);
endmodule
