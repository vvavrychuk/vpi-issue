#include <assert.h>
#include <string.h>
#include <vpi_user.h>

static int get_value(const char* name)
{
    vpiHandle handle = vpi_handle_by_name(name, NULL);
    assert(handle != NULL);

    s_vpi_value value;
    value.format = vpiIntVal;
    vpi_get_value(handle, &value);

    return value.value.integer;
}

static void set_value(const char* name, int value)
{
    vpiHandle handle = vpi_handle_by_name(name, NULL);
    assert(handle != NULL);

    s_vpi_value value_s;
    value_s.format = vpiIntVal;
    value_s.value.integer = value;

    s_vpi_time time;
    time.type = vpiSimTime;
    time.high = 0;
    time.low  = 0;

    vpi_put_value(handle, &value_s, &time, vpiInertialDelay);
}

static int clock_value_changed_cb(p_cb_data cb_data)
{
    if (cb_data->value->value.integer != 0) {
        set_value("tb_top.a", 1);
        set_value("tb_top.b", 1);
    }
}

static void register_clock_change_cb()
{
    s_cb_data cb_data = {0};
    cb_data.reason    = cbValueChange;
    cb_data.obj       = vpi_handle_by_name("tb_top.clk", NULL);
    cb_data.cb_rtn    = clock_value_changed_cb;

    s_vpi_time time   = {0};
    time.type         = vpiSimTime;
    cb_data.time      = &time;

    s_vpi_value value = {0};
    value.format      = vpiIntVal;
    cb_data.value     = &value;

    vpi_register_cb(&cb_data);
}

static int simulation_started_cb(p_cb_data cb_data)
{
    register_clock_change_cb();
}

static void startup_routine(void)
{
    s_cb_data cb_data = {0};
    cb_data.reason    = cbStartOfSimulation;
    cb_data.cb_rtn    = simulation_started_cb;
    vpi_register_cb(&cb_data);
}

void (*vlog_startup_routines[])() = 
{
    startup_routine,
    0
};
